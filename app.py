from flask import Flask
from flask_restful import Api

from services.Generate import Generate
from services.GetBase64 import GetBase64


app = Flask(__name__)
api = Api(app)

api.add_resource(Generate, "/generate/<titulo>/<subtitulo>/")
api.add_resource(GetBase64, "/getBase64/")


if __name__ == "__main__":
    app.run(debug=True)