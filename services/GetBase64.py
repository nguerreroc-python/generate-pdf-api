from flask import Flask, jsonify
from flask_restful import Resource, reqparse
from jinja2 import Environment, BaseLoader
import pdfkit
import base64

from mock_data import mockData


class GetBase64(Resource):

    requestParser = reqparse.RequestParser()

    def get(self):
        self.requestParserConfig()
        parseArgs = self.requestParser.parse_args()
        htmlTemplate = mockData.htmlTemplate
        templateModel = {
            "titulo": parseArgs["titulo"],
            "subtitulo": parseArgs["subtitulo"]
        }
        options = {
            "page-size": "A4",
            "margin-top": "0.1in",
            "margin-right": "0.1in",
            "margin-bottom": "0.1in",
            "margin-left": "0.1in",
        }

        template = Environment(loader=BaseLoader).from_string(htmlTemplate)
        html = template.render(templateModel)
        pdf = pdfkit.from_string(html, False, options)

        pdfEncoded = base64.b64encode(pdf)
        pdfBase64String = pdfEncoded.decode('utf-8')
        response = jsonify({"pdfFile": pdfBase64String})

        return response

    def requestParserConfig(self):
        self.requestParser.add_argument('titulo', type=str, required=True )
        self.requestParser.add_argument('subtitulo', type=str, required=True )
