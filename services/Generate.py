from flask import Flask, make_response
from flask_restful import Resource, reqparse
from jinja2 import Environment, BaseLoader
import pdfkit

from mock_data import mockData


class Generate(Resource):

    def get(self, titulo, subtitulo):
        htmlTemplate = mockData.htmlTemplate

        templateModel = {
            "titulo": titulo,
            "subtitulo": subtitulo
        }
        options = {
            "page-size": "A4",
            "margin-top": "0.1in",
            "margin-right": "0.1in",
            "margin-bottom": "0.1in",
            "margin-left": "0.1in",
        }

        template = Environment(loader=BaseLoader).from_string(htmlTemplate)
        html = template.render(templateModel)
        pdf = pdfkit.from_string(html, False, options)

        response = make_response(pdf)
        response.headers["Content-Type"] = "application/pdf"
        response.headers["Content-Disposition"] = "attanchment; filename=output.pdf"

        return response
